Git commands:


1. To create git repository for our folder, run command on the terminal:

$ git init

- git init stands for git initialized, it will create a hidden '.git' folder inside of our folder
- this '.git' contains all the necessary3 files and folders to run the git repository and track the changes of our folder.
- git init is executed only once

2. To check the status of the untracked files of our folder, run the command:

$ git status
- tracks the files that are not yet saved on our git repository

3. Then to add the changes or the revisions on the staging area, run the command:

$ git add -A

- -A (flag), stands for 'all'
- git add . vs git add -A => There's no difference at all
- adds all the tracked files/ changes on our staging area

3. To save/commit our history to our git repository, run command:

$ git commit -m "Initial Commit"

- git commit, saves the history on our git repository
- -m (flag), stands for 'message', it is required everytime we commit our changes
- -m flag must always contain a message written enclosed in a double quote
- messages is used to help us developers to identify what are the changes that we have made on our commits/history
- we add a message "Initial Commit" since its our FIRST time to save our version to our git repository

4. If your git repository has returned a fatal error with "*** Please tell me who you are.",
this only means that the git repository does not know us yet,
so we wanna make sure that we set up our accounts identity on our git repository, to do that run the command below:

$ git config --global user.name "Your Name" 

	ex: git config --global user.name "Christine Llapitan"
  
$ git config --global user.email "Your Email"

	ex: git config --global user.email "christine.llapitan@tuitt.com"
  
- we do this to tell our git repository who we are
- to check if our user name and user email are set up properly we can run the command below:

$ git config --global user.name

- this will return the set up name for git repository

$ git config --global user.email

- this will return the set up email for the git repository
- to change the set up user name and email, we can run the command below:

$ git config --global user.name "Your NEW name"

- and

$ git config --global user.email "Your NEW email"

5. To check the histories made on our git repositories, run command:

$ git log


-------------------------------------------------------------------------------------
								Setting up SSH keys on local machine and gitlab accounts
-------------------------------------------------------------------------------------
*Set up SSH keys first to make a secured connection between gitlab and local machines

1. To generate ssh-keys on our local machine, open your terminal/git bash then run command:

$ ssh-keygen

- After triggering this command, questionnaires will appear, just press 'Enter' until the QR code like will appear.
- The QR code like is a passphrase which will act as our pass/key for our local machine

2. Aligned to the passphrase we received from ssh-keygen, we will generate another one for our gitlab account, to do that, run command:

*For Windows User*

$ cat ~/.ssh/id_rsa.pub | clip

*For Mac User*

$ pbcopy < ~/.ssh/id_rsa.pub

- trigerring this command will generate an ssh key that we will use for our gitlab account
- the 'clip' keyword for windows command line stands for 'copied to clipboard'
- the 'pbcopy' keyword for mac command line is the equivalent of 'clip' in windows
- this command will not return anything, but to make sure that we have generated the ssh-key
open a notepad and hit ctrl+v (paste), once there are alphanumeric character appeared, then your ssh key is successful.

3. To add the ssh-key generated on our gitlab account, login first to your gitlab, 
then click the profile image (located on the upper right corner, *yung circle profile picture*), then select the 'Edit profile' on the options.

*sample link: https://gitlab.com/-/profile

4. Then inside your profile settings, look for the 'SSH Keys' on the left menu panel.
*sample link: https://gitlab.com/-/profile/keys

5. On the SSH Keys, you will see a textarea (text box), paste inside your generated ssh-key, add a title, leave the 'Expiry at' blank. 
Then press 'Add key' button

6. Then, there you have it, you ssh key now is established between your local machine and gitlab account

Important Note: 
*Once an error occured while adding the ssh key on our gitlab account, just go back to step no. 1 til step 6
*Make sure that there are no unnecessary spaces when pasting the ssh key on the text box on your gitlab account
*Leaving the 'Expiry at' blank will not set an expiration date on your ssh key and setting it for forever use. (Buti pa ssh key may forever </3)

-------------------------------------------------------------------------------------
								Uploading local repository on your remote repository
-------------------------------------------------------------------------------------
1. Login to your gitlab account, then on your dashboard, click 'New Project'
* or just go to this link: https://gitlab.com/projects/new

2. On the 'New Project' page,select 'Create blank project'
* or just go to this link: https://gitlab.com/projects/new#blank_project

3. On the 'Create blank project' page, fill up the project name 's02-d1', retain the visibility level private (for now), 
then uncheck the 'Initial repository with a README'.
	- the project name must be align to your local repository name, since our discussion is under session 02 we named the gitlab repository 's02-d1'
  - for now we will retain the private visibility level of our project
  - we uncheck the 'Initial repository with a README' to prevent the gitlab from creating a unnecessary README file on our remote repository,
  we want our repository to be empty since we will use it to contain the uploads from our local repository

4. Then on the created gitlab remote repository, look for 'Clone' button and click it.
5. Select the 'Clone with SSH', copy the url under it.
	- We will use the url from the 'Clone with SSH' since we established our local machine-gitlab connection using SSH
	- We will use this url as our destination address for our local repository

6. To establish our remote repository url address to our local repository, on your terminal under our present working directory (s02/d1), run command:

$ git remote add origin <gitlab url>

sample : $ git remote add origin git@gitlab.com:tuitt/students/batch131/resources/s02-d1.git

- git remote add - means we will add and connect a remote repository to our local repository
- origin is our alias to our gitlab url
- <gitlab url> - paste the copied gitlab url from our remote repository.

7. After setting the remote repository to our local repository, let's check if it successful, run the command:

$ git remote -v

- -v means verbose, returns a message
- the command will return the connected remote repository to our local
sample output:
	origin  git@gitlab.com:tuitt/students/batch131/resources/s02-d1.git (fetch)
	origin  git@gitlab.com:tuitt/students/batch131/resources/s02-d1.git (push)

 - fetch and push only means that this is the way of git repository to communicate to our remote repository, to fetch means, to download, and to push means, to upload
 
8. To upload our local repository to our remote repository, run command:

 $ git push origin master
 
 - git push means upload
 - origin is our alias to our destination url/address, where the local repository should be uploaded
 - master, this is the master/main container of the repository.
 
 9. To check if it is successfully uploaded, go back to your gitlab repository then refresh it.



-------- Terminologies and equivalent words ------------------
SSH keys = middleman, secured connection between our local machine and gitlab account
repository = folder
local repository = local folder which is located local machines
remote repository =  remote folder which is located online or on our Gitlab project
git push = upload our local repository to our remote repository
git pull = download our remote repository to our local repository
git commit = save changes to our git repository

